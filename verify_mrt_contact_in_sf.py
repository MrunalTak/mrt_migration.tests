import sys
import pandas as pd
from colorama import Fore


from sf_acct_contct_helper import get_sf_contact_for_emp_id as sf_contact
from sf_acct_contct_helper import mrt_spreadsheet_helper as mrt_sheet
from sf_acct_contct_helper import get_sf_account_id_for_each_contact as sf_acct_id
from sf_acct_contct_helper import get_sf_name_for_each_site as sf_acct_name
# from utilities_helper import get_sf_account_for_site_column

import logging
import datetime

localtime = datetime.datetime.now()

logging.basicConfig(filename='../Logs/mrt_contact_log_file.log',filemode='w',level=logging.INFO)

csv_to_read = mrt_sheet()

# get the list of empID column only & pass that to create array of employee_ids
fields_to_Choose = ['Provider','EmpID','Site','HomeDepartment']
mrt_file = pd.read_csv(csv_to_read, sep=',', usecols=fields_to_Choose, dtype=object, na_filter=False)
mrt_file = mrt_file.fillna(0)
records_from_mrt = mrt_file.drop_duplicates(['EmpID'])


# for each emp_id - connect to sf and check if it exists in sf.
logging.info('START - Logging for MRT Migration - CONTACTS & ACCOUNTS')
logging.info(localtime)
x_records = 0

for row in records_from_mrt.itertuples():
#   x_index = getattr(row, 'Index')
    x_records = x_records + 1
    emp_id_value = getattr(row, 'EmpID')
    site_value = getattr(row, 'Site')
    provider_value = getattr(row, 'Provider')
    home_department_value = getattr(row, 'HomeDepartment')
    home_department_value = home_department_value.encode('ascii', 'ignore')
    print ("--------------- PRocessing Row No#  " + str(x_records) + " of MRT spreadsheet")
    if not emp_id_value:
        continue
    else:
#Step1 - get EmpId from SF for each contact that exists
        return_contact_id_and_lname = sf_contact(emp_id_value)
#        return_contact_id_lname = return_contact_id_lname.encode('ascii','ignore')
        if return_contact_id_and_lname is None:
            print (Fore.RED + str(emp_id_value) + ' Contact record does NOT exist in SalesForce')
            logging.info(str(emp_id_value) + ' Contact record does NOT exist in SalesForce')
            continue
        else:
            emp_id = return_contact_id_and_lname[0]
            provider_lname = return_contact_id_and_lname[1]
            emp_id = emp_id.encode('ascii', 'ignore')
            provider_lname = provider_lname.encode('ascii', 'ignore')
            print (Fore.GREEN + str(emp_id) + ' Contact record exists in Salesforce')
#            logging.info(str(emp_id) + ' Contact record exists in Salesforce')
#Step2 - Check if the provider column has correct Lastname value.
            if (provider_lname and provider_value):
                provider_value = str(provider_value).capitalize()
                provider_lname = str(provider_lname).capitalize()
                if provider_value.find(provider_lname) > -1:
                    print (Fore.GREEN + str(provider_lname) + ' lastname of provider ' + str(provider_value) + ' verified in Salesforce')
#                    logging.info(str(provider_lname) + ' lastname of provider ' + str(provider_value) + ' verified in Salesforce')
                else:
                    print (Fore.GREEN + str(provider_lname) + ' lastname of provider ' + str(provider_value) + ' does Not exists in Salesforce')
                    logging.info(str(provider_lname) + ' lastname of provider ' + str(provider_value) + ' does Not exists in Salesforce')
#Step4 - Select if the siteID from scpreadsheet matches the value in SalesForce.
    if site_value:
        site_id_str = str(site_value)
        site_x_value = site_id_str[:4]
        if len(site_x_value) != 4:
            logging.info('Invalid value of site column in spreadsheet ' + str(site_x_value))
            print (Fore.YELLOW + 'Invalid value of site column in spreadsheet ' + str(site_x_value))
            continue
        else:
#Step5 - Check if site_id from spreadsheet exists in SF
            return_acct_id_value = sf_acct_id(emp_id)
            if return_acct_id_value :
#Step6 - Check if account name exists in SF for valid site_id
                acct_id_value = return_acct_id_value.encode('ascii', 'ignore')
#Step 7 - Check if the site_id from xsheet exists in Salesforce
                return_site_name_value = sf_acct_name(return_acct_id_value)
                if return_site_name_value:
                    return_site_name_value = return_site_name_value.encode('ascii','ignore')
                    print (str(return_site_name_value))
                    if home_department_value.find(return_site_name_value):
                        print (Fore.GREEN + str(home_department_value) + ' is verified in SaleForce for ' + str(site_x_value))
                    else:
                        print (Fore.RED + str(home_department_value) + ' does not exist in SaleForce for ' + str(site_x_value))
                        logging.info(str(home_department_value) + ' does not exist in SaleForce for ' + str(site_x_value))
                        continue

logging.info(localtime)
logging.info('END - Logging for MRT Migration - CONTACTS & ACCOUNTS')
