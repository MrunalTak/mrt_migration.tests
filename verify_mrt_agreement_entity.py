import sys
from Helpers import mrt_file_utility_helper as file_helper
from Helpers import mrt_to_sf_verification as mrt_sf_verfy
from Helpers import log_helper as lg_hlpr
from Helpers import mrt_columns_config as mrt_col_helpr
from Helpers import sf_query_helper as sf_u_helpr
from colorama import Fore
from Helpers import mrt_pay_frequency_key_file as pay_frq_file
# from mrt_pay_frequency_key_file import pay_frequency
import datetime


# Step 1 - Read MRT File row by row.
# Step 2 - For each lookup from spreadsheet, get the values from SF via salesforce api
# Step 3 - Verify value of spreadsheet with the SF data.
# Step 4 - Log the verification.
ret_log_msg = ""
pay_freq = 0
# Step 1 - Read MRT File row by row.
start_time = datetime.datetime.now()

log_msg = "\n START : Logging for MRT Migration - " + str(start_time)
lg_hlpr.log_results(log_msg, 'BLUE')

mrt_dict, num_mrt_rows = file_helper.read_mrt_record()
mrt_other_dict = file_helper.read_mrt_other_data_tab()


# Do this intermediate step to get RecordTypeId for unspecified recordType.
sf_recordType_id = sf_u_helpr.get_sf_recordtype_id()
if sf_recordType_id is "":
    lg_hlpr.log_results("\n ROW : MRT Migration - No Record Type with Unspecified Type ", 'RED')
    sys.exit(1)

sf_candidate_record_type_ids = sf_u_helpr.get_sf_candidate_record_types()
if sf_candidate_record_type_ids is "":
    lg_hlpr.log_results("\n ROW : MRT Migration - No Record Type with Unspecified Type ", 'RED')
    sys.exit(1)

# Step 2 - For each lookup from spreadsheet, get the values from SF via salesforce api

for index in range(num_mrt_rows):
    emp_found = 0
    mrt_record = mrt_col_helpr.set_mrt_single_record(mrt_dict,index)
    # print("\n MRT Migration - The record " + str(mrt_record))
    Emp_ID_Global_ID_mrt_value = mrt_record['Emp_ID_Global_ID'].strip()
    site_mrt_value = mrt_record['Site'].strip()
    log_msg = "\n ROW : MRT Migration - Looping through the MRT record number = " + str(index)
    lg_hlpr.log_results(log_msg, 'BLUE')

    candidate_id_sf_val = sf_u_helpr.get_candidate_id(Emp_ID_Global_ID_mrt_value, site_mrt_value, sf_candidate_record_type_ids)

    contact_id_sf_val = sf_u_helpr.get_contact_id(Emp_ID_Global_ID_mrt_value)

    if contact_id_sf_val == 0:
        ret_log_msg = "\t- ROW : MRT EmpID : " + Emp_ID_Global_ID_mrt_value + " contact does not exist in Salesforce"
        lg_hlpr.log_results(ret_log_msg, 'RED')
    elif candidate_id_sf_val == 0:
        ret_log_msg = "\t- ROW : MRT EmpID : " + Emp_ID_Global_ID_mrt_value + " with SiteID: "+  site_mrt_value + " does not exist in Salesforce"
        lg_hlpr.log_results(ret_log_msg, 'RED')
        # return_agrmt_sf_record = 0
    else:
        ret_pass_log_msg = "\t- ROW : MRT EmpID :  " + Emp_ID_Global_ID_mrt_value + " exists in Salesforce with SiteID: " + site_mrt_value
        lg_hlpr.log_results(ret_pass_log_msg, 'BLUE')

        if mrt_other_dict:
            for index, emp in enumerate(mrt_other_dict['Emp_ID_Global_ID']):
                if emp == mrt_record['Emp_ID_Global_ID']:
                    # print ('Emp_ID_Global_ID:'+ emp + 'index:' + str(index) + "Site:" + str(mrt_other_dict['Site'][index]) + "Fre:" + str(mrt_other_dict['Pay_Interval'][index]))
                    if mrt_other_dict['Site'][index]:
                        site_mrt_value = (mrt_other_dict['Site'][index].strip())[:6]
                        if site_mrt_value == mrt_record['Site']:
                            emp_found = 1
                            pay_freq_id = mrt_other_dict['Pay_Interval'][index]
                            pay_freq = pay_frq_file.pay_frequency(pay_freq_id)
                            if pay_freq is None:
                                pay_freq = 0
                                frq_log_msg = "\t- Column : MRT EmpID :  " + Emp_ID_Global_ID_mrt_value + "\t- FAIL - Pay Interval is invalid or missing in Other Data Tab."
                                lg_hlpr.log_results(frq_log_msg, 'LIGHTRED_EX')

                            mrt_sf_verfy.test_matching_columns(mrt_record, contact_id_sf_val, candidate_id_sf_val, pay_freq)
# same level as if mrt_other_dict:
        if emp_found != 1:
            frq_log_msg = "\t- Column : MRT EmpID :  " + Emp_ID_Global_ID_mrt_value + "\t- FAIL - is missing in Other Data Tab."
            lg_hlpr.log_results(frq_log_msg, 'LIGHTRED_EX')


end_time = datetime.datetime.now()
log_msg = "\n END : Logging for MRT Migration - " + str(end_time)
lg_hlpr.log_results(log_msg, 'BLUE')
