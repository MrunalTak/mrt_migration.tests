

from sf_agreement_config import set_query_for_sf
import mrt_sheet_header_config as headers
from sf_query_helper import *
from Helpers import log_helper as lg_hlpr




def test_matching_columns(mrt_single_record, contact_id , candidate_id, pay_freq):
        str_query = ""
        sf_recordType_id = get_sf_recordtype_id()

        # - Get all provider agreement for that candidate.
        provdr_agrmt_records = get_provider_agreements(contact_id)

        if not provdr_agrmt_records:
            str_log_provdr_agrmt = "\t- Column : " + mrt_single_record['Emp_ID_Global_ID'] + "\t- FAIL -Provider Agreement Does NOT exist."
            lg_hlpr.log_results(str_log_provdr_agrmt, 'LIGHTRED_EX')
            return
        else:
            str_log_provdr_agrmt = "\t- Column : " + mrt_single_record['Emp_ID_Global_ID'] + "\t- INFO - Provider Agreement exists."
            lg_hlpr.log_results(str_log_provdr_agrmt, 'LIGHTBLACK_EX')

            #    Get CURRENT provider agreement.
            provider_agrmt_dic, active_pa_count, current_pa = get_provdr_agrmnt_dic(provdr_agrmt_records, sf_recordType_id)

            if active_pa_count != 1:
                str_invalid_pa_count = "\t- Column : Invalid number of active/current provider agreements : " + \
                                       str(active_pa_count) + " for " + mrt_single_record['Emp_ID_Global_ID']

                lg_hlpr.log_results(str_invalid_pa_count, 'LIGHTRED_EX')
                return
            # else:
            #     #    Get EXPIRED provider agreement. This function is **NOT used anywhere**
            #     exp_agrmt = get_expired_pa(contact_id, current_pa)

        for header_col in headers.master_list:
            if header_col == "Shift_Hourly":
                continue
            else:
                # len_dic = len(provider_agrmt_dic)
                for index, agrms_value in provider_agrmt_dic.items():
                    # str_pro = "\t- Column : REMOVE provider_agrmt_dic: index " + str(index)
                    # lg_hlpr.log_results(str_pro, 'YELLOW')

                #     If this is MRT record, do the current record checkup.
                    if agrms_value['Is_Mrt_Record'] == 'Yes':
                        ending_agrmt_id = 'null'
                        str_query, active_rec, mrt_final_amount = set_query_for_sf(mrt_single_record, header_col, candidate_id, ending_agrmt_id, pay_freq)
                        if str_query != "":
                            validate_current_sf_agrmnt_term(str_query, header_col, active_rec, mrt_final_amount)
                            # str_log_msg, colornm = validate_current_sf_agrmnt_term(str_query, header_col, active_rec)
                            # lg_hlpr.log_results(str_log_msg, colornm)
                        else:
                            lg_hlpr.log_results("\t- Column : REMOVE - Not sure what I should do here?????  - in test_matching_columns 1 ", 'RED')
                    else:
                        # log_str = "\t- Column : " + str(header_col) + "\t- Record is not current MRT migrating record. "
                        # lg_hlpr.log_results(log_str, 'LIGHTRED_EX')
# Check here for each of expired PA, the start & end Term date is <= today
                        ending_agrmt_id = current_pa
                        # log_str = "\t- Column : " + header_col + "\t Remove - In Is_MRT_Record NO Logic - ending_agrmt_id : " + str(ending_agrmt_id)
                        # lg_hlpr.log_results(log_str, 'YELLOW')
                        str_exp_query, active_rec, final_amt = set_query_for_sf(mrt_single_record, header_col, candidate_id,
                                                                 ending_agrmt_id, pay_freq)
                        if str_exp_query != "":
                                validate_expired_sf_agrmnt_term(str_exp_query, header_col)
                        else:
                            lg_hlpr.log_results("\t- Column : REMOVE Not sure what I should do here????? - In test_matching_columns 2", 'RED')


def get_provdr_agrmnt_dic(d, sf_recordType_id):
    index = 0
    active_pa_count = 0
    current_pa = ""
    nested_dic = {}
    str_contract_stg = " and Contract_Stage__c  = 'Final Agreement Executed'"
    while index < len(d):
        pvr_agr_dic = {}
        pvr_agr_dic['Id'] = (d[index]['Id']).encode('ascii', 'ignore')
        pvr_agr_dic['Contract_Stage__c'] = d[index]['Contract_Stage__c']
        pvr_agr_dic['RecordTypeId'] = (d[index]['RecordTypeId']).encode('ascii', 'ignore')
        pvr_agr_dic['Agreement_Type__c'] = d[index]['Agreement_Type__c'].encode('ascii', 'ignore')
        if ((sf_recordType_id == pvr_agr_dic['RecordTypeId']) and (pvr_agr_dic['Agreement_Type__c'] == 'Unspecified') and (pvr_agr_dic['Contract_Stage__c'] == 'Final Agreement Executed')):
        #     It means it is unspecified & hence current MRT migration record.
            pvr_agr_dic['Is_Mrt_Record'] = 'Yes'
            active_pa_count += 1
            current_pa = pvr_agr_dic['Id']
        else:
            pvr_agr_dic['Is_Mrt_Record'] = 'No'

        nested_dic[index] = pvr_agr_dic
        index += 1
        # msg1 = "   REMOVE -- The returning provider agreement is " + str(pvr_agr_dic)
        # lg_hlpr.log_results(msg1, 'YELLOW')
    return nested_dic, active_pa_count, current_pa




