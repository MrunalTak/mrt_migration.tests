from colorama import Fore
from Helpers import sf_query_helper as sf_u_helpr
from log_helper import log_results
from mrt_pay_frequency_key_file import pay_frequency as p_frq
from mrt_file_utility_helper import read_mrt_other_data_tab as other_tab
from decimal import Decimal

def set_query_for_sf(mrt_record_dict, header_column, candidate_id, end_agrmt_number, pay_freq):

    mrt_final_amount = 0.00
    query = ""
    hard_coded_dic = {}
    col_value_exists = "N"
    mrt_amount = 0.00

    hard_coded_dic['Candidate_Id'] =  candidate_id
    sf_recordType_id = sf_u_helpr.get_sf_recordtype_id()

    if end_agrmt_number == "null":
        mrt_amt_to_check = mrt_record_dict[header_column]
        if mrt_amt_to_check != "":
                mrt_amt_chk = str(mrt_amt_to_check).strip()
                if mrt_amt_chk.__contains__(","):
                    mrt_amt_chk = str((mrt_amt_chk).replace(",", ""))

                if mrt_amt_chk.__contains__("$"):
                    mrt_amt_chk = str((mrt_amt_chk).replace("$", ""))


                mrt_amount =  Decimal(mrt_amt_chk)
                col_value_exists = "Y"
        # else:
            # If there is no amount in the spreadsheet, send the query with no amount. And make sure that the ageement terms that are returned (if any) have term_end_date <= today.
            # mrt_prnt_str = "\t- Column : "+ header_column + "\t- INFO - Column is empty in spreadsheet."
            # # mrt_amt_string = ""
            # log_results(mrt_prnt_str, 'LIGHTBLACK_EX')
    # else:
        # mrt_amt_string = ""

    # msg1 = "  mrt_amount is " + str(mrt_amount)
    # log_results(msg1, 'MAGENTA')

    if header_column == "Salary":
        hard_coded_dic['Description__c'] = 'Clinical Salary'
        hard_coded_dic['short_code__c'] = 'CLSAL'
        hard_coded_dic['type__c'] = 'Clinical Salary'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic,  end_agrmt_number)
    #
    elif header_column == "Day_Rate":
        hard_coded_dic['Description__c'] = "Day Shift"
        hard_coded_dic['short_code__c'] = "Day"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Night_Rate":
        hard_coded_dic['Description__c'] = "Night Shift"
        hard_coded_dic['short_code__c'] = "Night"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic,  end_agrmt_number)

    elif header_column == "Swing_Rate":
        hard_coded_dic['Description__c'] = "Swing Shift"
        hard_coded_dic['short_code__c'] = "SWING"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Wknd_Day_Rate":
        hard_coded_dic['Description__c'] = "Weekend Day Shift"
        hard_coded_dic['short_code__c'] = "WKNDD"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic,  end_agrmt_number)
    #
    elif header_column == "Wknd_Night_Rate":
        hard_coded_dic['Description__c'] = "Weekend Night Shift"
        hard_coded_dic['short_code__c'] = "WKNDN"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Wknd_Swing_Rate":
        hard_coded_dic['Description__c'] = "Weekend Swing Shift"
        hard_coded_dic['short_code__c'] = "WKNDS"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Admin_Rate":
        hard_coded_dic['Description__c'] = "Admin Shift"
        hard_coded_dic['short_code__c'] = "ADMIN"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "APP_Supervision_Shift":
        hard_coded_dic['Description__c'] = "APP Supervision"
        hard_coded_dic['short_code__c'] = "APPSU"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Backup_Extra_Shift":
        hard_coded_dic['Description__c'] = "Working Excess"
        hard_coded_dic['short_code__c'] = "EXCSS"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Call_In_Shift":
        hard_coded_dic['Description__c'] = "Call In Shift"
        hard_coded_dic['short_code__c'] = "CLLIN"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Clinic_Shift":
        hard_coded_dic['Description__c'] = "Clinic Shift"
        hard_coded_dic['short_code__c'] = "CLINC"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Flex_Shift":
        hard_coded_dic['Description__c'] = "Flex Shift"
        hard_coded_dic['short_code__c'] = "FLEX"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "ICU_Shift":
        hard_coded_dic['Description__c'] = 'ICU Shift'
        hard_coded_dic['short_code__c'] = 'ICU'
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "ICU_Night_Shift":
        hard_coded_dic['Description__c'] =  'ICU Night Shift'
        hard_coded_dic['short_code__c'] = 'ICUN'
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Jeopardy_Shift":
        hard_coded_dic['Description__c'] = "Jeopardy Shift"
        hard_coded_dic['short_code__c'] = "JEOP"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Lead_Rate":
        hard_coded_dic['Description__c'] = "Lead Shift"
        hard_coded_dic['short_code__c'] = "LEAD"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Lead_OT":
        hard_coded_dic['Description__c'] = 'Lead Overtime'
        hard_coded_dic['short_code__c'] =  'LEADO'
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Orientation_Shift":
        hard_coded_dic['Description__c'] = 'Orientation Shift'
        hard_coded_dic['short_code__c'] = 'ORNTN'
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Paid_Time_Off_Rate":
        hard_coded_dic['Description__c'] = 'PTO'
        hard_coded_dic['short_code__c'] = 'PTO'
        hard_coded_dic['type__c'] = 'PTO/Sick'
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Palliative_Shift":
        hard_coded_dic['Description__c'] = "Palliative Shift"
        hard_coded_dic['short_code__c'] = 'PALLI'
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "PAT_Shift":
        hard_coded_dic['Description__c'] = "PAT Shift"
        hard_coded_dic['short_code__c'] = 'PAT'
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Sick_Rate":
        hard_coded_dic['Description__c'] = 'Sick'
        hard_coded_dic['short_code__c'] = 'SICK'
        hard_coded_dic['type__c'] = 'PTO/Sick'
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Teaching_Rate":
        hard_coded_dic['Description__c'] = "Teaching Shift"
        hard_coded_dic['short_code__c'] = "TEACH"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Teaching_Night_Shift":
        hard_coded_dic['Description__c'] = "Teaching Night Shift"
        hard_coded_dic['short_code__c'] = "TEACN"
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Telemed_Remote_Rate":
        hard_coded_dic['Description__c'] = "Telemedicine/Remote Shift"
        hard_coded_dic['short_code__c'] = 'TELE'
        hard_coded_dic['type__c'] = check_shift_or_hourly(mrt_record_dict['Shift_Hourly'])
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)


    #
    elif header_column == "Admin_Stipend":
        hard_coded_dic['Description__c'] = "Admin Stipend"
        hard_coded_dic['short_code__c'] = "ADMST"
        hard_coded_dic['type__c'] = "Stipend"
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "APP_Director_Stipend":
        hard_coded_dic['Description__c'] = "APP Director Stipend"
        hard_coded_dic['short_code__c'] = "APDST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Asst_Med_Dir_Stipend":
        hard_coded_dic['Description__c'] = "Asst Med Director Stipend"
        hard_coded_dic['short_code__c'] = "AMDST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Chief_Stipend":
        hard_coded_dic['Description__c'] = "Chief Stipend"
        hard_coded_dic['short_code__c'] = "CHFST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Compass_Stipend":
        hard_coded_dic['Description__c'] = "Compass Stipend"
        hard_coded_dic['short_code__c'] = "CMPSS"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Hospitalist_Council_Stipend":
        hard_coded_dic['Description__c'] = "Hospitalist Council Stipend"
        hard_coded_dic['short_code__c'] = "HOCST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "APP_Director_Stipend":
        hard_coded_dic['Description__c'] = "APP Director Stipend"
        hard_coded_dic['short_code__c'] = "APDST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Leadership_Stipend":
        hard_coded_dic['Description__c'] = "Leadership Stipend"
        hard_coded_dic['short_code__c'] = "LDPST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Med_Dir_Stipend":
        hard_coded_dic['Description__c'] = "Med Director Stipend"
        hard_coded_dic['short_code__c'] = "MDSTP"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Medical_Education_Stipend":
        hard_coded_dic['Description__c'] = "Medical Education Stipend"
        hard_coded_dic['short_code__c'] = "MEDED"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Med_Ins_Stipend__TIG":
        hard_coded_dic['Description__c'] = "Med Ins Stipend (TIG)"
        hard_coded_dic['short_code__c'] = "MISTP"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "NPP__Stipend":
        hard_coded_dic['Description__c'] = "NPP Stipend"
        hard_coded_dic['short_code__c'] = "NPPST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "PCP_Relations_Stipend":
        hard_coded_dic['Description__c'] = "PCP Stipend"
        hard_coded_dic['short_code__c'] = "PCPST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Program_Director_Stipend":
        hard_coded_dic['Description__c'] = "Program Director Stipend"
        hard_coded_dic['short_code__c'] = "PDRST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Residency_Stipend":
        hard_coded_dic['Description__c'] = "Residency Stipend"
        hard_coded_dic['short_code__c'] = "RESTI"
        hard_coded_dic['type__c'] = "Stipend"
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "RMD_Stipend":
        hard_coded_dic['Description__c'] = "RMD Stipend"
        hard_coded_dic['short_code__c'] = "RMDST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
    #
    elif header_column == "Scheduling_Stipend":
        hard_coded_dic['Description__c'] = "Scheduling Stipend"
        hard_coded_dic['short_code__c'] = "SCSTI"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)

    elif header_column == "Seniority_Stipend":
        hard_coded_dic['Description__c'] = "Seniority Stipend"
        hard_coded_dic['short_code__c'] = "SRSTI"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic, end_agrmt_number)
#
    elif header_column == "Teaching_Stipend":
        hard_coded_dic['Description__c'] = "Teaching Stipend"
        hard_coded_dic['short_code__c'] = "TEAST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic,  end_agrmt_number)

    elif header_column == "Travel_Stipend":
        hard_coded_dic['Description__c'] = "Travel Stipend"
        hard_coded_dic['short_code__c'] = "TRAST"
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic,  end_agrmt_number)

    elif header_column == "Other_Stipend":
        hard_coded_dic['Description__c'] = "Other Stipend"
        hard_coded_dic['short_code__c'] = 'STPND'
        hard_coded_dic['type__c'] = 'Stipend'
        exp_sf_amt = calculate_sf_amount(hard_coded_dic, mrt_amount, pay_freq)
        mrt_final_amount = exp_sf_amt
        query_str = form_a_query(hard_coded_dic,  end_agrmt_number)
    #
    elif header_column == "Treadmill":
        hard_coded_dic['Description__c'] = "Treadmill"
        hard_coded_dic['short_code__c'] = 'TREAD'
        hard_coded_dic['type__c'] = 'Work Unit Pay'
        mrt_final_amount = mrt_amount
        query_str = form_a_query(hard_coded_dic,  end_agrmt_number)
    else:
        query_str = ""

    return query_str, col_value_exists, mrt_final_amount


def check_shift_or_hourly(is_shift_hourly):
    if (is_shift_hourly == 'S') or (is_shift_hourly == 's'):
        return "Shift Pay"
    else:
        return "Hourly Pay"

def form_a_query(hard_coded_dic, ending_agrmt_id):
    # sf_recordType_id = sf_u_helpr.get_sf_recordtype_id()
    # if hard_coded_dic:
    if ending_agrmt_id == "null":
        str_mt_guidlines =  " and Meets_Guideline__c = 'Yes'"
        query = "SELECT  short_code__c, Term_Start_Date__c, Term_End_Date__c, Starting_Agreement_CT__c, Amount__c, Interval__c FROM Agreement_Term__c Where Candidate__c = '" + str(
            hard_coded_dic['Candidate_Id']) + "' and Short_Code__c='" + str(
            hard_coded_dic['short_code__c']) + "' and Description__c ='" + str(
            hard_coded_dic['Description__c']) + "' and Type__c='" + str(
            hard_coded_dic['type__c']) + "' and Ending_Agreement_CT__c = " + ending_agrmt_id  + str_mt_guidlines
            # hard_coded_dic['type__c']) + "' and Ending_Agreement_CT__c = " + ending_agrmt_id + str(
            # amt_str) + str_mt_guidlines   -- ORIGINAL - Removed + str(amt_str)

        # msg2 = "\t REMOVE - The Query to get terms  is: " + query
        # log_results(msg2, 'MAGENTA')
    else:
        # This is expired terms.
        query = "SELECT  short_code__c, Term_Start_Date__c, Term_End_Date__c, Starting_Agreement_CT__c FROM Agreement_Term__c Where " \
                "Candidate__c = '" + str(hard_coded_dic['Candidate_Id']) + \
                "' and Short_Code__c='" + str(hard_coded_dic['short_code__c']) + \
                "' and Type__c='" + str(hard_coded_dic['type__c']) + \
                "' and Starting_Agreement_CT__c != null and Ending_Agreement_CT__c ='" + ending_agrmt_id + "' "


    # msg1 = "\t REMOVE - The Query to get expired terms  is: " + query
    # log_results(msg1, 'MAGENTA')
    return query


def calculate_sf_amount(hard_coded_dict, mrt_amt, pay_freq):
    amt_with_frq = Decimal(0.00)
    if mrt_amt:
        amt_with_frq = Decimal(mrt_amt) * pay_freq
            # msg2 = "\t REMOVE - The value Frequency is :: " + str(pay_freq) + "\t passed dict is :: " + str(
            #     hard_coded_dict) + "\t MRT string is :: " + str(mrt_amt_string) + "\t MRT amount is ::" + str(amt_with_frq)
            # log_results(msg2, 'MAGENTA')
        # Version 1 (that worked mostly except .48
    # amt_with_frq = round(amt_with_frq)
    # amt_with_frq = Decimal(amt_with_frq)
         # Version 2 March17
    amt_with_frq_1 = round(amt_with_frq, 2)
    amt_with_frq_2 = str(amt_with_frq_1)
    amt_with_frq = Decimal(amt_with_frq_2)

    return amt_with_frq


