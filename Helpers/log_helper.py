import sys
import logging
import datetime

from colorama import Fore

localtime = datetime.datetime.now()

file_date = '{0:_%m-%d_%H-%M}'.format(localtime)
str_file_name = 'C:/Git-Sp/mrt_migration.tests/Logs/mrt_Agreement_log_QAData' + str(file_date) + '.log'
logging.basicConfig(filename=str_file_name,filemode='w',level=logging.INFO, format='%(message)s', datefmt='%m/%d/%Y %I:%M %p')

def log_results(log_msg, colorname = 'GREEN'):
    color = getattr(Fore, colorname)

    logging.info(log_msg)
    print(color + log_msg)

    return


