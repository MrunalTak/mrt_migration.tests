import sys
import csv
from colorama import Fore
# import logging
import datetime
from Helpers import log_helper as lg_hlpr

localtime = datetime.datetime.now()


# mrt_file = 'C:/Git-Sp/mrt_migration.tests/Helpers/data_files/500_MRT_Master_Tab_03_07.csv'
mrt_file = 'C:/Git-Sp/mrt_migration.tests/Helpers/data_files/TEST_MRT_Master_Tab_03_07_V1.csv'

# mrt_file = 'C:/Git-Sp/mrt_migration.tests/Helpers/data_files/File1_MRT_Master_Tab_03_07_V1.csv'
# mrt_file = 'C:/Git-Sp/mrt_migration.tests/Helpers/data_files/MRT_Master_Tab_03_07_V1.csv'
mrt_other_data_file = 'C:/Git-Sp/mrt_migration.tests/Helpers/data_files/MRT_Other_Data_Tab_03_07_V1.csv'


# COlumns to read Emp_ID_Global_ID,Site,Pay_Interval
def read_mrt_record():
    num_rows = 0
    with open(mrt_file) as csv_file:
        mrt_reader = csv.DictReader(csv_file, skipinitialspace=True, delimiter=',')
        mrt_header = mrt_reader.fieldnames
        mrt_dict = {name: [] for name in mrt_reader.fieldnames}
        log_msg = "\n ROW : MRT Migration - Processing file :" + str(mrt_file)
        lg_hlpr.log_results(log_msg, 'BLUE')
        for row in mrt_reader:
            num_rows += 1
            for name in mrt_reader.fieldnames:
                mrt_dict[name].append(row[name])

    return mrt_dict, num_rows


def read_mrt_other_data_tab():
    mrt_o_dict = []
    row_nums = 0
    with open(mrt_other_data_file) as csv_file:
        mrt_o_reader = csv.DictReader(csv_file, skipinitialspace=True, delimiter=',')
        mrt_o_header = mrt_o_reader.fieldnames

        mrt_o_dict = {colmns: [] for colmns in mrt_o_header}
        for row in mrt_o_reader:
            row_nums += 1
            for colmns in mrt_o_header:
                mrt_o_dict[colmns].append(row[colmns].strip())


    return mrt_o_dict
