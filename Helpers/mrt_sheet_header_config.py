

# OUT OF SCOPE COLUMNS ON MRT are PRS,  Ultipro ID , Primary Site, Job title, Region, Division,  Of Shifts Before ML,EE Info,Base Pay, From Master Sheet, ML Rule ,  ML Day Rate, ML Night Rate, ML Swing Rate, ML Wknd Day Rate, ML Wknd Night Rate, ML Wknd Swing Rate, ML Tiers Description, Moonlighting Verified, Other_shifts    , "Worked_Holiday_Shift_Premium_Rate"


master_list = [
    # "Lookup"
    # , "PRS"
    # , "Ultipro_ID"
    # , "Provider"
    # , "Emp_ID_Global_ID"
    # , "Site"
    # , "Primary_Site"
    # , "Job_title"
    "Shift_Hourly"
    # , "Region"
    # , "Division"
    # , "Of_Shifts_Before_ML"
    # , "EE_Info"
    , "Salary"
    , "Day_Rate"
    , "Night_Rate"
    , "Swing_Rate"
    , "Wknd_Day_Rate"
    , "Wknd_Night_Rate"
    , "Wknd_Swing_Rate"
    # , "Base_Pay"
    # , "ML_Rule"
    # , "ML_Day_Rate"
    # , "ML_Night_Rate"
    # , "ML_Swing_Rate"
    # , "ML_Wknd_Day_Rate"
    # , "ML_Wknd_Night_Rate"
    # , "ML_Wknd_Swing_Rate"
    # , "ML_Tiers_Description"
    # , "Moonlighting"
    , "Admin_Rate"
    , "APP_Supervision_Shift"
    , "Backup_Extra_Shift"
    , "Call_In_Shift"
    , "Clinic_Shift"
    , "Flex_Shift"
    # , "Holiday_Rate" TO REMOVE
    , "ICU_Shift"
    , "ICU_Night_Shift"
    , "Jeopardy_Shift"
    , "Lead_Rate"
    , "Lead_OT"
    , "Orientation_Shift"
    , "Paid_Time_Off_Rate"
    , "Palliative_Shift"
    , "PAT_Shift"
    , "Sick_Rate"
    , "Teaching_Rate"
    , "Teaching_Night_Shift"
    , "Telemed_Remote_Rate"
    , "Admin_Stipend"
    , "APP_Director_Stipend"
    , "Asst_Med_Dir_Stipend"
    , "Chief_Stipend"
    , "Compass_Stipend"
    , "Hospitalist_Council_Stipend"
    , "Leadership_Stipend"
    , "Med_Dir_Stipend"
    , "Medical_Education_Stipend"
    , "Med_Ins_Stipend__TIG"
    , "NPP__Stipend"
    , "PCP_Relations_Stipend"
    , "Program_Director_Stipend"
    , "Residency_Stipend"
    , "RMD_Stipend"
    , "Scheduling_Stipend"
    , "Seniority_Stipend"
    , "Teaching_Stipend"
    , "Travel_Stipend"
    , "Other_Stipend"
    # # , "Quality_Withhold_Ddct"
    , "Treadmill"
    # , "Recurring"
    # , "Questions_Notes"
]