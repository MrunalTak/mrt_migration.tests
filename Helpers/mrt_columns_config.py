from decimal import Decimal

def set_mrt_single_record(mrt_dict, index):

    mrt_sheet_record = {}

    Emp_ID_Global_ID_mrt_value = (mrt_dict['Emp_ID_Global_ID'][index]).encode('ascii', 'ignore')
    mrt_sheet_record['Emp_ID_Global_ID'] = Emp_ID_Global_ID_mrt_value

    # site_mrt_value = (mrt_dict['Site'][index])[:4] + '.' + (mrt_dict['Site'][index])[4]

    site_mrt_value = (mrt_dict['Site'][index].strip())[:6]
    mrt_sheet_record['Site'] = site_mrt_value

    shift_hourly_mrt_value = mrt_dict['Shift_Hourly'][index].strip()
    mrt_sheet_record['Shift_Hourly'] = shift_hourly_mrt_value

    salary_mrt_value = mrt_dict['Salary'][index].strip()
    mrt_sheet_record['Salary'] = salary_mrt_value

    # Day_Rate
    day_rate_mrt_value = mrt_dict['Day_Rate'][index].strip()
    mrt_sheet_record['Day_Rate'] = day_rate_mrt_value

    # Night_Rate
    night_rate_mrt_value = mrt_dict['Night_Rate'][index].strip()
    mrt_sheet_record['Night_Rate'] = night_rate_mrt_value

    # Swing_Rate
    swing_rate_mrt_value = mrt_dict['Swing_Rate'][index].strip()
    mrt_sheet_record['Swing_Rate'] = swing_rate_mrt_value

    #Wknd_Day_Rate
    wknd_day_rate_mrt_value = mrt_dict['Wknd_Day_Rate'][index].strip()
    mrt_sheet_record['Wknd_Day_Rate'] = wknd_day_rate_mrt_value

    #Wknd_Night_Rate
    wknd_night_rate_mrt_value = mrt_dict['Wknd_Night_Rate'][index].strip()
    mrt_sheet_record['Wknd_Night_Rate'] = wknd_night_rate_mrt_value

    #Wknd_Swing_Rate
    wknd_swing_rate_mrt_value = mrt_dict['Wknd_Swing_Rate'][index].strip()
    mrt_sheet_record['Wknd_Swing_Rate'] = wknd_swing_rate_mrt_value

    #Admin_Rate
    admin_rate_mrt_value = (mrt_dict['Admin_Rate'][index]).strip()
    mrt_sheet_record['Admin_Rate'] = admin_rate_mrt_value

    #APP_Supervision_Shift
    app_supervision_shift_mrt_value = mrt_dict['APP_Supervision_Shift'][index].strip()
    mrt_sheet_record['APP_Supervision_Shift'] = app_supervision_shift_mrt_value

    #Backup_Extra_Shift
    backup_extra_shift_mrt_value = mrt_dict['Backup_Extra_Shift'][index].strip()
    mrt_sheet_record['Backup_Extra_Shift'] = backup_extra_shift_mrt_value
    #
    call_in_shift_mrt_value = mrt_dict['Call_In_Shift'][index].strip()
    mrt_sheet_record['Call_In_Shift'] = call_in_shift_mrt_value
    #
    clinic_shift_mrt_value = mrt_dict['Clinic_Shift'][index].strip()
    mrt_sheet_record['Clinic_Shift'] = clinic_shift_mrt_value

    flex_shift_mrt_value = mrt_dict['Flex_Shift'][index].strip()
    mrt_sheet_record['Flex_Shift'] = flex_shift_mrt_value
    #
    icu_shift_mrt_value = mrt_dict['ICU_Shift'][index].strip()
    mrt_sheet_record['ICU_Shift'] = icu_shift_mrt_value
    #
    icu_night_shift_mrt_value = mrt_dict['ICU_Night_Shift'][index].strip()
    mrt_sheet_record['ICU_Night_Shift'] = icu_night_shift_mrt_value
    #
    jeopardy_shift_mrt_value = mrt_dict['Jeopardy_Shift'][index].strip()
    mrt_sheet_record['Jeopardy_Shift'] = jeopardy_shift_mrt_value
    #
    lead_rate_mrt_value = mrt_dict['Lead_Rate'][index].strip()
    mrt_sheet_record['Lead_Rate'] = lead_rate_mrt_value
    #
    lead_ot_mrt_value = mrt_dict['Lead_OT'][index].strip()
    mrt_sheet_record['Lead_OT'] = lead_ot_mrt_value
    #
    orientation_shift_mrt_value = mrt_dict['Orientation_Shift'][index].strip()
    mrt_sheet_record['Orientation_Shift'] = orientation_shift_mrt_value

    #Paid_Time_Off_Rate
    paid_time_off_rate_mrt_value = mrt_dict['Paid_Time_Off_Rate'][index].strip()
    mrt_sheet_record['Paid_Time_Off_Rate'] = paid_time_off_rate_mrt_value
    #
    palliative_shift_mrt_value = mrt_dict['Palliative_Shift'][index].strip()
    mrt_sheet_record['Palliative_Shift'] = palliative_shift_mrt_value
    #
    pat_shift_mrt_value = mrt_dict['PAT_Shift'][index].strip()
    mrt_sheet_record['PAT_Shift'] = pat_shift_mrt_value
    #
    sick_rate_mrt_value = mrt_dict['Sick_Rate'][index].strip()
    mrt_sheet_record['Sick_Rate'] = sick_rate_mrt_value
    # #
    teaching_rate_mrt_value = mrt_dict['Teaching_Rate'][index].strip()
    mrt_sheet_record['Teaching_Rate'] = teaching_rate_mrt_value
    #
    teaching_night_shift_mrt_value = mrt_dict['Teaching_Night_Shift'][index].strip()
    mrt_sheet_record['Teaching_Night_Shift'] = teaching_night_shift_mrt_value
    #
    telemed_remote_rate_mrt_value = mrt_dict['Telemed_Remote_Rate'][index].strip()
    mrt_sheet_record['Telemed_Remote_Rate'] = telemed_remote_rate_mrt_value
    #

    #
    admin_stipend_mrt_value = mrt_dict['Admin_Stipend'][index].strip()
    mrt_sheet_record['Admin_Stipend'] = admin_stipend_mrt_value

    app_director_stipend_mrt_value = mrt_dict['APP_Director_Stipend'][index].strip()
    mrt_sheet_record['APP_Director_Stipend'] = app_director_stipend_mrt_value


    asst_med_dir_stipend_mrt_value = mrt_dict['Asst_Med_Dir_Stipend'][index].strip()
    mrt_sheet_record['Asst_Med_Dir_Stipend'] = asst_med_dir_stipend_mrt_value

    #
    chief_stipend_mrt_value = mrt_dict['Chief_Stipend'][index].strip()
    mrt_sheet_record['Chief_Stipend'] = chief_stipend_mrt_value

    #Compass_Stipend
    compass_stipend_mrt_value = mrt_dict['Compass_Stipend'][index].strip()
    mrt_sheet_record['Compass_Stipend'] = compass_stipend_mrt_value

    #Hospitalist_Council_Stipend
    hospitalist_council_stipend_mrt_value = mrt_dict['Hospitalist_Council_Stipend'][index].strip()
    mrt_sheet_record['Hospitalist_Council_Stipend'] = hospitalist_council_stipend_mrt_value

    #Leadership_Stipend
    leadership_stipend_mrt_value = mrt_dict['Leadership_Stipend'][index].strip()
    mrt_sheet_record['Leadership_Stipend'] = leadership_stipend_mrt_value

    #Med_Dir_Stipend
    med_dir_stipend_mrt_value = mrt_dict['Med_Dir_Stipend'][index].strip()
    mrt_sheet_record['Med_Dir_Stipend'] = med_dir_stipend_mrt_value

    #Medical_Education_Stipend
    medical_education_stipend_mrt_value = mrt_dict['Medical_Education_Stipend'][index].strip()
    mrt_sheet_record['Medical_Education_Stipend'] = medical_education_stipend_mrt_value

    #Med_Ins_Stipend__TIG
    med_ins_stipend__TIG_mrt_value = mrt_dict['Med_Ins_Stipend__TIG'][index].strip()
    mrt_sheet_record['Med_Ins_Stipend__TIG'] = med_ins_stipend__TIG_mrt_value

    #NPP__Stipend
    npp__stipend_mrt_value = mrt_dict['NPP__Stipend'][index].strip()
    mrt_sheet_record['NPP__Stipend'] = npp__stipend_mrt_value

    #PCP_Relations_Stipend
    pcp_relations_stipend_mrt_value = mrt_dict['PCP_Relations_Stipend'][index].strip()
    mrt_sheet_record['PCP_Relations_Stipend'] = pcp_relations_stipend_mrt_value

    #Program_Director_Stipend
    program_director_stipend_mrt_value = mrt_dict['Program_Director_Stipend'][index].strip()
    mrt_sheet_record['Program_Director_Stipend'] = program_director_stipend_mrt_value

    # Residency_Stipend
    residency_stipend_mrt_value = mrt_dict['Residency_Stipend'][index].strip()
    mrt_sheet_record['Residency_Stipend'] = residency_stipend_mrt_value

    #RMD_Stipend
    rmd_stipend_mrt_value = mrt_dict['RMD_Stipend'][index].strip()
    mrt_sheet_record['RMD_Stipend'] = rmd_stipend_mrt_value

    #Scheduling_Stipend
    scheduling_stipend_mrt_value = mrt_dict['Scheduling_Stipend'][index].strip()
    mrt_sheet_record['Scheduling_Stipend'] = scheduling_stipend_mrt_value

    #Seniority_Stipend
    seniority_stipend_mrt_value = mrt_dict['Seniority_Stipend'][index].strip()
    mrt_sheet_record['Seniority_Stipend'] = seniority_stipend_mrt_value

    #Teaching_Stipend
    teaching_stipend_mrt_value = mrt_dict['Teaching_Stipend'][index].strip()
    mrt_sheet_record['Teaching_Stipend'] = teaching_stipend_mrt_value

    #Travel_Stipend
    travel_stipend_mrt_value = mrt_dict['Travel_Stipend'][index].strip()
    mrt_sheet_record['Travel_Stipend'] = travel_stipend_mrt_value
    #
    # #Other_Stipend
    other_stipend_mrt_value = mrt_dict['Other_Stipend'][index].strip()
    mrt_sheet_record['Other_Stipend'] = other_stipend_mrt_value
    #
    # # Treadmill
    treadmill_mrt_value = mrt_dict['Treadmill'][index].strip()
    mrt_sheet_record['Treadmill'] = treadmill_mrt_value
    #

    return mrt_sheet_record

