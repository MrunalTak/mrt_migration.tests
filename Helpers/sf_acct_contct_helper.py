from sf_connection_helper import sf
import logging
from colorama import Fore
from datetime import date

today = date.today()



def mrt_spreadsheet_helper():
#    file_to_read = '../Helpers/myfile_0818_copy_small.csv'
    file_to_read = '../Helpers/myfile_0818_copy_medium_100.csv'
#    file_to_read = '../Helpers/myfile_0818_copy_medium.csv'
#    file_to_read = '../Helpers/myfile_0818_copy.csv'
    print file_to_read
    return file_to_read


def db_setup():
    return

def get_sf_contact_for_emp_id(employee_id):
    results = sf.query("SELECT EmpId__c, LastName FROM Contact where EmpID__c = '" + str(employee_id) + "'")
    records = results['records']
    if not records:
#        print (Fore.RED + str(employee_id) + ' employee ID does NOT exist in SalesForce')
        return_id = None
    else:
        dicts = records[0]
        emp_id_c = dicts['EmpID__c']
        provider_last_name = dicts['LastName']
#        print(Fore.GREEN + str(emp_id_c) + ' employeeID exists in SalesForce.' )
        return_id = (emp_id_c, provider_last_name)

    return return_id



def get_sf_account_id_for_each_contact(contact_value):
    account_query = "SELECT AccountId FROM Contact WHERE  EmpID__c = '" + str(contact_value) + "'"
    raw_results = sf.query(account_query)
    records = raw_results['records']
    if not records:
        return_account_id = None
    else:
        account_dict = records[0]
        contact_account_id = account_dict['AccountId']
        return_account_id = contact_account_id
    return return_account_id

# def get_sf_account_for_site_column(site_column):
#     site_query = "SELECT Id from Sound_Service_lines__c WHERE Site_Code__c LIKE '" + str(site_column) + "%'"
#     raw_results = sf.query(site_query)
#     records = raw_results['records']
#     if not records:
#         return_id = None
#     else:
#         dicts = records[0]
#         id = dicts['Id']
#         return_id = id
#     return return_id


def get_sf_name_for_each_site(hospital_partner_value):
    name_query = "SELECT Name from Account WHERE Id = '" + str(hospital_partner_value) + "'"
    result_name = sf.query(name_query)
    records = result_name['records']
    if not records:
        return_name = None
    else:
#        site_name = records
        dicts = records[0]
        site_name = dicts['Name']
        return_name = site_name
    return return_name

