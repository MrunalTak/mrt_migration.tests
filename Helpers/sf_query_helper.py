from sf_connection_helper import sf
import logging
from colorama import Fore
from sf_connection_helper import sf
from datetime import date
from Helpers import log_helper as log
from decimal import Decimal , ROUND_DOWN


today = date.today()
str_unspecified = "Unspecified"
sf_recordType_id = ""

def get_sf_recordtype_id():
    record_type_query = "SELECT Id FROM RecordType as rt where rt.name LIKE '%" + str_unspecified + "%' "
    result_name = sf.query(record_type_query)
    records = result_name['records']
    if not records:
        record_type_id = ""
    else:
        record_type_dic = records[0]
        sf_recordType_id = record_type_dic['Id']

    return sf_recordType_id


def get_contact_id(emp_id_value):
    contact_query = "Select Id from Contact WHERE EmpID__c = '" + str(emp_id_value) + "'"
    result_name = sf.query(contact_query)
    records = result_name['records']
    if not records:
        ret_contact_id = 0
    else:
        contact_dict = records[0]
        ret_contact_id = contact_dict['Id'].encode('ascii', 'ignore')

    return ret_contact_id


def get_sf_candidate_record_types():
    candidt_record_type_ids = []

    candidt_record_type = "Select Id FROM RecordType where name IN ('w2' , 'W2 - Priv', 'Cross Credential', 'Cross Credential - Priv' )"
    result_name = sf.query(candidt_record_type)
    records = result_name['records']
    if not records:
        candidt_record_type_ids = ""
    else:
        for item in records:
            candidate_type = item['Id'].encode('ascii', 'ignore')
            candidt_record_type_ids.append(candidate_type)


    return candidt_record_type_ids


def get_candidate_id(emp_id_value, site_value, sf_candidate_record_type_ids):

    site_value = str(site_value).strip()

    str_can_value = str(sf_candidate_record_type_ids).replace("]", "")
    string_record_type_ids = str(str_can_value).replace("[", "")

    # Version 1 - Used till March 20
    # emp_query = "Select Id FROM Candidate__c WHERE Contact_EmpID__c = '" + str(emp_id_value) + "' AND " + " cost_center__c like '" + str(site_value) + "%' "

    # Version 2 - Updating to use  ('w2', 'W2 - Priv','Cross Credential','Cross Credential - Priv')


    emp_query = "Select Id FROM Candidate__c WHERE Contact_EmpID__c = '" + str(emp_id_value) + "' AND " + " cost_center__c like '" + str(site_value) + "%' and Service_line_Site_Code__c LIKE '" + str(site_value)[:4] + "%' AND RecordTypeId IN (" + str(string_record_type_ids) + ") order by Active__c desc"
    # TODO: Error out when there are more than one candidate recortTYpes are 1. w2, 2. w2 - PRIV 3. Cross credential 4. Cross Credential - PRIV (One of thise four recordType, otherwise error out) _ Dev to take care of it.
    result_name = sf.query(emp_query)
    records = result_name['records']
    if not records:
        return_candidate_id = 0
    else:
        dicts = records[0]
        return_candidate_id = dicts['Id'].encode('ascii', 'ignore')
    return return_candidate_id

def get_provider_agreements(contact_id):

    provdr_agrmt_query = "SELECT pa.Id, pa.RecordTypeId, pa.Contract_Stage__c, Agreement_Type__c from Provider_Agreement__c as pa WHERE pa.contact__c = '" + contact_id + "' "

    result_name = sf.query(provdr_agrmt_query)
    provdr_agrmt_records = result_name['records']
    # msg1 = "\t- -- REMOVE -- In get_provider_agreements  " + provdr_agrmt_query
    # log.log_results(msg1, 'RED')

    return provdr_agrmt_records

def get_expired_pa(contact_id, current_pa):
    exp_provdr_agrmt_query = "SELECT pa.Id from Provider_Agreement__c as pa " \
                         " WHERE pa.contact__c  = '" + contact_id + "' and pa.Id != '" + current_pa + "' "

    result_name = sf.query(exp_provdr_agrmt_query)
    exp_provdr_agrmt_records = result_name['records']

    if not exp_provdr_agrmt_records:
        str_no_expired_pa = "\t- Column : Old provider agreements do not exist."
        log.log_results(str_no_expired_pa, 'YELLOW')
        return
    else:
        # str_pvd = "Expired provider agreements  " + str(exp_provdr_agrmt_records)
        # log.log_results(str_pvd, 'YELLOW')
        return exp_provdr_agrmt_records




def validate_current_sf_agrmnt_term(agrmt_query, col,  exp_active_rec, mrt_final_amount):
    returning_log_msg = ""
    result_name = sf.query(agrmt_query)
    agrmt_records = result_name['records']
    colorname = 'LIGHTBLACK_EX'
    # msg2 = "  Column : Column Checking is " + col + " Query is " + agrmt_query
    # log.log_results(msg2, 'YELLOW')
    if not agrmt_records:
        if exp_active_rec == "N":
            returning_log_msg = "\t- Column : " + col + "\t- INFO - Migration not needed for empty MRT column."
            colorname = 'LIGHTBLACK_EX'
        elif exp_active_rec == "Y":
            returning_log_msg = "\t- Column : " + col + "\t- FAIL - MRT column value did not get migrated."
            colorname = 'LIGHTRED_EX'

        # return returning_log_msg
        # log.log_results(returning_log_msg, 'LIGHTRED_EX')   ORIGINAL
    elif agrmt_records:
        agrmt_record_num = len(agrmt_records)
        # If the record return ONE record, Term_End_Date__c  is NULL and Term_Start_Date__c is today's date
        # msg3 = "\t- REMOVE Column : No# of agrmt_records returned are " + str(agrmt_record_num)
        # log.log_results(msg3, 'YELLOW')

        if agrmt_record_num == 1:
            agrmt_dic = agrmt_records[0]
            returning_log_msg, colorname = one_record_verification(agrmt_dic,exp_active_rec, col, mrt_final_amount)
        elif agrmt_record_num > 1:
            # str_log_1 = "\t- Column REMOVE :  Multiple Records Found For : " + agrmt_records
            # log.log_results(str_log_1, 'LIGHTRED_EX')
            colorname = 'YELLOW'

            returning_log_msg = multi_record_verification(agrmt_records,exp_active_rec)

        # msg4 = " Column : REMOVE ???? " + str_log
    log.log_results(returning_log_msg, colorname)
    return


def validate_expired_sf_agrmnt_term(exp_pt_query, header_col):
    returning_log_msg = ""
    result_name = sf.query(exp_pt_query)
    exp_pt_records = result_name['records']
    # msg5 = "  Column : Expired col " + header_col + " Query formed is " + exp_pt_query
    # log.log_results(msg5, 'YELLOW')
    if exp_pt_records:
        # for exp_at_key, exp_at_value in exp_pt_records.items():
        for exp_at_key, exp_at_value in enumerate(exp_pt_records):
            # msg6 = "\t- Column : EXPIRED PROVIDER TERM KEY: " + str(exp_at_key)
            # log.log_results(msg6, 'YELLOW')
            # msg7 = "\t- Column : EXPIRED PROVIDER TERM VALUE: " + str(exp_at_value)
            # log.log_results(msg7, 'YELLOW')
        # for key in exp_at_value:
            exp_agrmt_term_start_date = exp_at_value['Term_Start_Date__c']
            exp_agrmt_term_end_date = exp_at_value['Term_End_Date__c']
            if ((exp_agrmt_term_start_date is not None) and (exp_agrmt_term_end_date is not None)):
                exp_agrmt_term_start_date = date(*map(int, exp_agrmt_term_start_date.split('-')))
                exp_agrmt_term_end_date = date(*map(int, exp_agrmt_term_end_date.split('-')))
                if ((exp_agrmt_term_end_date <= today) and (exp_agrmt_term_start_date <= today )):
                    returning_log_msg = "\t- Column : " + str(header_col) +  "\t- PASS - Ending Agreement Term updated correctly."
                    log.log_results(returning_log_msg, 'LIGHTBLACK_EX')
            else:
                returning_log_msg = "\t- Column : " + str(header_col) + "\t- FAIL - Ending Agreement Term did not update correctly. "
                log.log_results(returning_log_msg, 'LIGHTRED_EX')
    else:
        # returning_log_msg = "\t- Column : " +  str(header_col) + "\t- PASS - No Expired Agreement Terms. "
        # log.log_results(returning_log_msg, 'LIGHTBLACK_EX')

        return




def one_record_verification(agrmt_dic, exp_rec, column, final_amount_to_chk):
    returning_log_msg = ""
    clrname = 'LIGHTBLACK_EX'
    is_amt_match = 0
    sf_amount_annually = Decimal(0.00)
    # msg7 = " Column : current PA " + str(agrmt_dic)
    # msg8 = "  Column : Expired PA " + str(exp_rec)
    # log.log_results(msg7,'LIGHTBLACK_EX')
    # log.log_results(msg8,'RED')
    sf_agrmt_term_start_date = agrmt_dic['Term_Start_Date__c']
    sf_agrmt_term_end_date = agrmt_dic['Term_End_Date__c']
    sf_agrmt_starting_agreement_ct = agrmt_dic['Starting_Agreement_CT__c']
    sf_agrmt_col_check = agrmt_dic['Short_Code__c']
    # 3-17-adding Decimal
    sf_agrmt_amount_check = agrmt_dic['Amount__c']
    sf_agrmt_interval_check = agrmt_dic['Interval__c']


    if sf_agrmt_interval_check is not None:
        if sf_agrmt_interval_check == 'Monthly':
            multiply_sf_amt_by = 12
        elif sf_agrmt_interval_check == 'Quarterly':
                multiply_sf_amt_by = 4
        elif sf_agrmt_interval_check == 'Annually':
                multiply_sf_amt_by = 1
    else:
        multiply_sf_amt_by = 1

    if (sf_agrmt_amount_check is not None) and (final_amount_to_chk is not None):
        final_amount_to_chk = Decimal(final_amount_to_chk)
        # Version 1
        # sf_amount_annually = Decimal(sf_agrmt_amount_check) * multiply_sf_amt_by

        # Version 2
        # sf_amount_annually = round(Decimal((sf_agrmt_amount_check) * multiply_sf_amt_by), 2)
        # Version 3 March17
        multiplying_value = Decimal(sf_agrmt_amount_check) * multiply_sf_amt_by
        multiplying_value_1 = round(multiplying_value, 2)
        multiplying_value_2 = str(multiplying_value_1)
        sf_amount_annually = Decimal(multiplying_value_2)
        # Version A - Before March 17 - Comparison
        # if sf_amount_annually == final_amount_to_chk:
        #     is_amt_match = 1

        # Version B - March 17 - Comparison
        if sf_amount_annually == final_amount_to_chk:
            is_amt_match = 1
        else:
            # Version B plus April 04 - Added clause to check only Stipend columns calculations validation
            if ('Stipend' in column or 'Salary' in column):
                dc_diff = Decimal(sf_amount_annually) - Decimal(final_amount_to_chk)
                if (int(sf_amount_annually) == int(final_amount_to_chk) and dc_diff < 1):
                    # is_amt_match = 1 - per Lori 4/3
                    this_log_msg = "\t- Column : " + column + "\t- Column : SF calculated amount :" + str(sf_amount_annually) + "\t - MRT calculated amount :" +  str(final_amount_to_chk)
                    clrname = 'LIGHTRED_EX'
                    log.log_results(this_log_msg, clrname)
            # this_log_msg = "\t- Column : final_amount_to_chk: " + str(final_amount_to_chk)
            # log.log_results(this_log_msg, clrname)


    if sf_agrmt_term_end_date is not None:
        sf_agrmt_term_end_date = date(*map(int, sf_agrmt_term_end_date.split('-')))

    if sf_agrmt_term_start_date is not None:
        sf_agrmt_term_start_date = date(*map(int, sf_agrmt_term_start_date.split('-')))

    if exp_rec == "Y":
        if sf_agrmt_term_start_date <= today:
            if (str(sf_agrmt_col_check) == 'RESTI' or str(sf_agrmt_col_check) == 'RETBN' or str(sf_agrmt_col_check) == 'CMPSS' or str(sf_agrmt_col_check) == 'MDCST'):
                if sf_agrmt_term_end_date is not None:
                    if is_amt_match == 1:
                        returning_log_msg = "\t- Column : " + column + "\t- PASS - MRT Record Migrated Correctly. "
                        clrname = 'LIGHTBLACK_EX'
                    else:
                        returning_log_msg = "\t- Column : " + column + "\t- FAIL - MRT amount does not match salesforce value. "
                        clrname = 'LIGHTRED_EX'
                else:
                    returning_log_msg = "\t- Column : " + column + "\t- FAIL - MRT Record does not have correct Term_End_date."
                    clrname = 'LIGHTRED_EX'
            else:
                if sf_agrmt_term_end_date is None:
                    if is_amt_match == 1:
                        returning_log_msg = "\t- Column : " + column + "\t- PASS - MRT Record Migrated Correctly. "
                        clrname = 'LIGHTBLACK_EX'
                    else:
                        returning_log_msg = "\t- Column : " + column + "\t- FAIL - MRT amount does not match salesforce value. "
                        clrname = 'LIGHTRED_EX'
                else:
                    returning_log_msg = "\t- Column : " + column + "\t- FAIL - MRT Record does not have correct Term_End_date."
                    clrname = 'LIGHTRED_EX'
    elif exp_rec == "N":
        if sf_agrmt_term_start_date <= today:
            if sf_agrmt_term_end_date is None or sf_agrmt_term_end_date <= today:
                    # returning_log_msg = "\t- Column : " + column + "\t- PASS - Migration not needed for empty MRT column."
                    # clrname = 'LIGHTBLACK_EX'
                    if is_amt_match == 1:
                        returning_log_msg = "\t- Column : " + column + "\t- PASS - MRT Record Migrated Correctly. "
                        clrname = 'LIGHTBLACK_EX'
                    else:
                        returning_log_msg = "\t- Column : " + column + "\t- FAIL - MRT amount does not match salesforce value. "
                        clrname = 'LIGHTRED_EX'

    return returning_log_msg, clrname


def multi_record_verification(sf_dicts, exp_recs):
    returning_log_msg = "multi record Verification "
    log.log_results(returning_log_msg, 'YELLOW')
    return returning_log_msg
