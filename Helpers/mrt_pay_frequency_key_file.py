# Pay Group	Multiply Salary/Stipend By
# AMB10	12
# ATM10	12
# ATM15	12
# ATMS	24
# EGLSM	24
# EVR10	12
# EVRS	24
# GC10	12
# GRLK10	12
# GULF10	12
# GULFS	24
# ME10	12
# MES	24
# TELM10	12
# TIG5	12
# TIGS	24
import logging
from Helpers import log_helper as log


def pay_frequency(f_id):
    pay_freq_dict = {'AMB10': 12, 'ATM10': 12, 'ATM15': 12, 'ATMS': 24, 'EGLSM': 24, 'EVR10': 12, 'EVRS': 24,
                     'GC10': 12,
                     'GRLK10': 12, 'GULF10': 12, 'GULFS': 24, 'ME10': 12, 'MES': 24, 'TELM10': 12, 'TIG5': 12,
                     'TIGS': 24}

    pay_frq = pay_freq_dict.get(f_id)

    # msg1 = "\t- Column : PAY Frequency is " + str(pay_frq)
    # log.log_results(msg1, 'MAGENTA')

    return pay_frq